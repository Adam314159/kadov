---
title: ""
date: 2019-04-20T10:05:17+02:00
draft: false
---

{{< wrapper id="otabore" title="Letní tábor Kadov" subtitle="" >}}

<div style="width: 100%; background: rgba(180,120,50,0.3); padding: 10px; border-radius: 10px;">

<h3>Pro letošek už máme tábor za sebou</h3>
<br>
Ale budeme se na vás těšit v roce 2025!
<!--<br>Letos jedeme na tábor v termínu <b>6.-19. 8. 2023</b>
<br><br><b>Ke stažení:</b> <a href="/images/prihlasky/Prihlaska_2023.pdf" style="color: lightblue;" download>PŘIHLÁŠKA</a> a <a href="/images/prihlasky/Informace_2023.pdf" style="color: lightblue;" download>INFORMACE</a>
<br>
-->
</div>

<br>

<center><iframe class="video" src="https://www.youtube.com/embed/nKSMWAoVxZI?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></center><br><br><br><br>

<b>Pracujeme s dětmi od 8 do 15 let</b>
<br><br>

<div style="width: 100%; background: rgba(100,100,100,0.3); padding: 10px; border-radius: 10px;">
  
Náš tábor se koná pod záštitou TJ Sokol České Budějovice a letos to bude třináctý rok, kdy budeme tábořit v Kadově u Blatné, který nám poskytuje doslova malebnou přírodu, několik rybníků a dobré zázemí.
<br>
(<a href="https://mapy.cz/s/2xjfR" target="_blank" noreferrer noopener>ukázat na mapě</a>)



</div><br>


<a href="/images/taboriste/200.jpg" data-lightbox="taboriste" class="animate inline-block w-40 sm:w-64"><img src="/images/taboriste/nahl/200.png"></a>
<a href="/images/taboriste/2.jpg" data-lightbox="taboriste" class="animate inline-block w-40 sm:w-64"><img src="/images/taboriste/nahl/2.png"></a>
<a href="/images/taboriste/4.jpg" data-lightbox="taboriste" class="animate inline-block w-40 sm:w-64"><img src="/images/taboriste/nahl/4.png"></a>
<br><br><br>


<h1>Zázemí</h1>
<div style="width: 100%; background: rgba(100,100,100,0.3); padding: 10px; border-radius: 10px;">

Děti jsou ubytovány v dřevěných srubech se třemi místnostmi po třech až čtyřech postelích, krmíme je nejméně pětkrát denně a ve sprchách teče teplá voda. 
</div>
<br>
<a href="/images/taboriste/1.jpg" data-lightbox="zazemi" class="animate inline-block w-32 sm:w-48"><img src="/images/taboriste/nahl/1.png"></a>
<a href="/images/taboriste/13.jpg" data-lightbox="zazemi" class="animate inline-block w-32 sm:w-48"><img src="/images/taboriste/nahl/13.png"></a>
<a href="/images/taboriste/nastenka.jpg" data-lightbox="zazemi" class="animate inline-block w-32 sm:w-48"><img src="/images/taboriste/nahl/nastenka.png"></a>
<a href="/images/taboriste/rozcestnik.jpg" data-lightbox="zazemi" class="animate inline-block w-32 sm:w-48"><img src="/images/taboriste/nahl/rozcestnik.png"></a>
<a href="/images/taboriste/chatky.jpg" data-lightbox="zazemi" class="animate inline-block w-32 sm:w-48"><img src="/images/taboriste/nahl/chatky.png"></a>
<a href="/images/taboriste/kamelot.jpg" data-lightbox="zazemi" class="animate inline-block w-32 sm:w-48"><img src="/images/taboriste/nahl/kamelot.png"></a>
<a href="/images/taboriste/ohen.jpg" data-lightbox="zazemi" class="animate inline-block w-32 sm:w-48"><img src="/images/taboriste/nahl/ohen.png"></a>

<br><br>

<h1>Náš tábor</h1>
<div style="width: 100%; background: rgba(100,100,100,0.3); padding: 10px; border-radius: 10px;">

Každý náš tábor je věnován určitému tématu, které rámcuje veškeré dění na táboře (letos se můžete těšit na Cestu kolem světa za 80 dní. Učíme děti základním tábornickým dovednostem jako je práce se dřevem a ohněm, pohyb a bezpečnost v lese, samostatné rozhodování, spolupráce, řešení problémů a tak podobně. To vše formou her – jak klidových, tak akčních, jak ve dne, tak v noci. Důraz klademe na zážitkovou pedagogiku.
</div>  
 
<br>
<a href="/images/taboriste/12.jpg" data-lightbox="priroda" class="animate inline-block w-32 sm:w-48"><img src="/images/taboriste/nahl/12.png"></a>
<a href="/images/taboriste/vareni.jpg" data-lightbox="priroda" class="animate inline-block w-32 sm:w-48"><img src="/images/taboriste/nahl/vareni.png"></a> 
<a href="/images/taboriste/11.jpg" data-lightbox="priroda" class="animate inline-block w-32 sm:w-48"><img src="/images/taboriste/nahl/11.png"></a>
<a href="/images/taboriste/8.jpg" data-lightbox="priroda" class="animate inline-block w-32 sm:w-48"><img src="/images/taboriste/nahl/8.png"></a>
<a href="/images/taboriste/9.jpg" data-lightbox="priroda" class="animate inline-block w-32 sm:w-48"><img src="/images/taboriste/nahl/9.png"></a>
<a href="/images/taboriste/jirik.jpg" data-lightbox="priroda" class="animate inline-block w-32 sm:w-48"><img src="/images/taboriste/nahl/jirik.png"></a>
<a href="/images/taboriste/7.jpg" data-lightbox="priroda" class="animate inline-block w-32 sm:w-48"><img src="/images/taboriste/nahl/7.png"></a>
{{< endWrapper >}}



 
