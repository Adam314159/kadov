---
title: "Celotáborovka | "
date: 2019-04-20T10:05:17+02:00
draft: false
layout: celotaborovka
---
{{< lightbox >}}

<br>
{{< wrapper id="celotaborovka" title="" subtitle="" >}}

<div style="text-align: left; padding: 20px; margin: 20px; color: white; font-family: 'Montserrat', cursive; font-size: 20px;">

<br>

<center>

<img src="/images/ships.jpg" style="box-shadow: 0px 0px 15px 0px rgba(50,130,180);">
</center><br><br>

Je rok 3022 a lidstvo si dokázalo poradit s mnoha svými problémy! Aliance spojených planet odvážně cestuje vesmírem, kde kolonizuje jednu planetu za druhou. Součástí tohoto velkolepého díla můžeš být i ty! 
<br><br>
Po reportu průzkumné skupiny, byla druhá planeta v soustavě Epsilon Eridani shledána za vhodnou pro lidskou populaci. Vyraž s námi 6 - 19. srpna 2023 a staň se prospektorem! S Aliancí spojených planet pro lepší zítřky!

{{< endWrapper >}}



{{< endWrapper >}}





 
