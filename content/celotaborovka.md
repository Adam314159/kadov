---
title: "Celotáborovka | "
date: 2019-04-20T10:05:17+02:00
draft: false
layout: celotaborovka
---
{{< lightbox >}}

<br>
{{< wrapper id="celotaborovka" title="" subtitle="" >}}

<div style="text-align: left; padding: 20px; margin: 20px; color: white; font-size: 20px;">

<br>

<center>

<br><br>
<b>
Neuznaní podlou lest potajmu připravili,<br>
prastarou věštbu tak po věcích naplnili.<br>
Ztracený hrdina ze spánku procitá,<br>
neb jeho krev jest pro tvrdou mstu nezbytná.<br>
Krutá zrada se pokradmu chystá,<br>
obětí stane se duše přečistá.<br>
V lásce a věrnosti naděje zůstává,<br>
však duše hrdiny po pláních bloudívá.<br>
Poslední veliká bitva se blíží,<br>
zhoubou či smrtí nový věk klíčí.<br>
</b>
</center>
<br><br>
„Hrdinové! V hlubinách Tartaru se něco začíná hýbat, na svět se vracejí bájní tvorové, kteří již po věky nespatřili světlo světa a s sebou přinášejí chaos a zkázu. Proto vás vyzývám, obrňte se, vyzbrojte se a přijďte za námi do tábora polokrevných. Společně zaženeme nestvůry zpátky tam, kam patří – do temných hlubin Tartaru!“

{{< endWrapper >}}



{{< endWrapper >}}





 
