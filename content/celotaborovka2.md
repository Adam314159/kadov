---
title: "Celotáborovka | "
date: 2019-04-20T10:05:17+02:00
draft: false
layout: celotaborovka
---
{{< lightbox >}}

<br><br><br>
{{< wrapper id="celotaborovka" title="" subtitle="" >}}

<h1>Vesmírná odysea</h1>

<div style="text-align: left; padding: 20px; margin: 20px; color: white; font-family: 'Montserrat', cursive; font-size: 20px;">

<br>

<center><iframe class="video" src="https://www.youtube.com/embed/dItcypunrwU?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></center><br><br>

Celá posádka mojí mezihvězdné lodi se vydala na dlouhou cestu nebezpečným vesmírem. Na palubě je sice doprovází nejlepší astronauti, ale vesmír je nebezpečné místo. Neznámý svět, který je naším cílem, může skrývat mnoho rizik. Naší posádku čeká náročný úkol! Budu se těšit, až se shledáme.

{{< endWrapper >}}



{{< endWrapper >}}





 
