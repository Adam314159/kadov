---
title: "Foto | "
date: 2019-04-20T10:05:17+02:00
draft: false
---


  {{< wrapper id="foto" title="Foto" subtitle="" >}}
<div style="width: 100%; background: rgba(100,100,100,0.3); padding: 10px; border-radius: 10px;">
Tady je jen pár vybraných fotek, chcete-li vidět více, hledejte prosím na našem <a href="https://www.facebook.com/letnitaborkadov/photos/" target="_blank">facebooku</a>.
</div><br> <br> 

<a href="/images/foto/bozi.jpg" data-lightbox="priroda" class="animate inline-block w-40 sm:w-48"><img src="/images/foto/nahl/bozi.png"></a>
<a href="/images/foto/deti.jpg" data-lightbox="priroda" class="animate inline-block w-40 sm:w-48"><img src="/images/foto/nahl/deti.png"></a>
<a href="/images/foto/stromy.jpg" data-lightbox="priroda" class="animate inline-block w-40 sm:w-48"><img src="/images/foto/nahl/stromy.png"></a>
<a href="/images/foto/stul.jpg" data-lightbox="priroda" class="animate inline-block w-40 sm:w-48"><img src="/images/foto/nahl/stul.png"></a>  
<a href="/images/foto/1.jpg" data-lightbox="priroda" class="animate inline-block w-40 sm:w-48"><img src="/images/foto/nahl/1.png"></a>
<a href="/images/foto/3.jpg" data-lightbox="priroda" class="animate inline-block w-40 sm:w-48"><img src="/images/foto/nahl/3.png"></a>
<a href="/images/foto/6.jpg" data-lightbox="priroda" class="animate inline-block w-40 sm:w-48"><img src="/images/foto/nahl/6.png"></a>
<a href="/images/foto/7.jpg" data-lightbox="priroda" class="animate inline-block w-40 sm:w-48"><img src="/images/foto/nahl/7.png"></a>
<a href="/images/foto/8.jpg" data-lightbox="priroda" class="animate inline-block w-40 sm:w-48"><img src="/images/foto/nahl/8.png"></a>
<a href="/images/foto/9.jpg" data-lightbox="priroda" class="animate inline-block w-40 sm:w-48"><img src="/images/foto/nahl/9.png"></a>
<a href="/images/foto/10.jpg" data-lightbox="priroda" class="animate inline-block w-40 sm:w-48"><img src="/images/foto/nahl/10.png"></a>
<a href="/images/foto/13.jpg" data-lightbox="priroda" class="animate inline-block w-40 sm:w-48"><img src="/images/foto/nahl/13.png"></a>
<a href="/images/foto/14.jpg" data-lightbox="priroda" class="animate inline-block w-40 sm:w-48"><img src="/images/foto/nahl/14.png"></a>
<a href="/images/foto/15.jpg" data-lightbox="priroda" class="animate inline-block w-40 sm:w-48"><img src="/images/foto/nahl/15.png"></a>
<a href="/images/foto/16.jpg" data-lightbox="priroda" class="animate inline-block w-40 sm:w-48"><img src="/images/foto/nahl/16.png"></a>
<a href="/images/foto/17.jpg" data-lightbox="priroda" class="animate inline-block w-40 sm:w-48"><img src="/images/foto/nahl/17.png"></a>
<a href="/images/foto/18.jpg" data-lightbox="priroda" class="animate inline-block w-40 sm:w-48"><img src="/images/foto/nahl/18.png"></a>
<a href="/images/foto/24.jpg" data-lightbox="priroda" class="animate inline-block w-40 sm:w-48"><img src="/images/foto/nahl/24.png"></a>
<a href="/images/foto/25.jpg" data-lightbox="priroda" class="animate inline-block w-40 sm:w-48"><img src="/images/foto/nahl/25.png"></a>
<a href="/images/foto/27.jpg" data-lightbox="priroda" class="animate inline-block w-40 sm:w-48"><img src="/images/foto/nahl/27.png"></a>
<a href="/images/foto/28.jpg" data-lightbox="priroda" class="animate inline-block w-40 sm:w-48"><img src="/images/foto/nahl/28.png"></a>
<a href="/images/foto/43.jpg" data-lightbox="priroda" class="animate inline-block w-40 sm:w-48"><img src="/images/foto/nahl/43.png"></a>
<a href="/images/foto/46.jpg" data-lightbox="priroda" class="animate inline-block w-40 sm:w-48"><img src="/images/foto/nahl/46.png"></a>
<a href="/images/foto/47.jpg" data-lightbox="priroda" class="animate inline-block w-40 sm:w-48"><img src="/images/foto/nahl/47.png"></a>
<a href="/images/foto/48.jpg" data-lightbox="priroda" class="animate inline-block w-40 sm:w-48"><img src="/images/foto/nahl/48.png"></a>
<a href="/images/foto/49.jpg" data-lightbox="priroda" class="animate inline-block w-40 sm:w-48"><img src="/images/foto/nahl/49.png"></a>
<a href="/images/foto/50.jpg" data-lightbox="priroda" class="animate inline-block w-40 sm:w-48"><img src="/images/foto/nahl/50.png"></a>  
  
  
  {{< endWrapper >}}







 
