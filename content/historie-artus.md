---
title: "Historie | "
date: 2019-04-20T10:05:17+02:00
draft: false
---


  {{< wrapper id="" title="2008 - Král Artuš videa" subtitle="" >}}
<center>
Starodávne video, ze starodávného foťáku, zachycující souboj hrdinného Lancelota proti rozmazlenému Artušovi (ale on se poučí, nebojte).<br><br> 
<iframe class="video" src="https://www.youtube.com/embed/ChTbibVpGx4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe><br>
Po dlouhém putování za svatým grálem se ho pár vyvoleným podařilo spatřit<br><br> 
<iframe class="video" src="https://www.youtube.com/embed/hPHmftWXRfk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe><br>
</center>

  {{< endWrapper >}}







 
