---
title: "Historie | "
date: 2019-04-20T10:05:17+02:00
draft: false
---


  {{< wrapper id="kontakt" title="2018 - Japonsko videa" subtitle="" >}}
<center>
Zatím poslední z mistrů, jež přislíbili svou účast, je slečna Natsumi. Slečna Natsumi vede skromnou jídelnu ve vesničce Tsurui, avšak z celé říše se sjíždí znalci a neváhají obětovat dlouhou cestu, jen aby ochutnali její věhlasné sushi. Právě díky své profesi pak Natsumi měla možnost naučit se hráti Go a vytříbit své znalosti utkáním s mnoha různými hráči z celé řady míst. I proto je její styl velmi přizpůsobivý, což ji činí nebezpečným soupeřem.<br><br> 
<iframe class="video" src="https://www.youtube.com/embed/xbTLq60ympg?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe><br>

<br><br><br>
Jedním z posledních mistrů, které má tato osoba tu čest Vám představit, je sám náš pán, nejváženější šogun, Hideyoshi-sama. Náš pán je velikým hráčem Go a jedním z nejuznávanějších mistrů této hry v dějinách říše. To, že si s vítězem našeho turnaje zahraje partii, je jednou z největších možných poct a tato osoba si této příležitosti nesmírně váží.<br><br>
<iframe class="video" src="https://www.youtube.com/embed/jyoxTfd2Atc?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> 
<br><br><br>


Zejména pro své nevídané lékařské schopnosti a ochotu pomáhat si Mistr Jarní Potůček získala oblibu mnoha. A přesto, že starodávnému umění origami se věnuje celá řada umělců, jen málokdo z nich by mohl tvrdit, že se Jarnímu Potůčku může rovnat. Než se přihlásila do turnaje, nebylo o Jarním Potůčku, přes její oblíbenost, nějakou dobu příliš slyšet. O to více mě těší, že se s touto významnou osobností budu moci setkat tváří v&nbsptvář.<br><br> 
<iframe class="video" src="https://www.youtube.com/embed/8VYEStoebxA?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe><br><br><br>

Aoši-san je dalším z mistrů, kteří se probojovali až na turnaj. Této osobě není o jeho minulosti mnoho známo a Aoši sám je muž nemnoha slov. Není však nejmenších pochyb o tom, že na turnaji je zaslouženě. Na desce vyniká nečekanými, vychytralými tahy a často i zdánlivě špatnou pozici dokáže obrátit ve svůj prospěch. Nejpozoruhodnější však je, že Aoši-san ovládá mnoho technik záhadného umění ninjutsu, které je obvykle drženo v naprosté tajnosti a předáváno jen v nejužším kruhu zasvěcených. Podaří-li se nám ho přesvědčit aby nám pár technik poodhalil, mohlo by to být nanejvýš poučné. <br><br>
<iframe class="video" src="https://www.youtube.com/embed/PXrN3UHqsbg?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe><br><br><br>


Mistr Dýně je velmi vážená osobnost provincie Kadó. Poustevník většinu roku sídlící vysoko v horách, který je živ jenom z darů rýže, jež mu nosí lidé, co k němu chodí pro radu. Mistr dýně je jedním z posledních znalců dnes již téměř zapomenutého umění boje s naginatou a zcela legendární hráč Go. Vyniká trpělivostí a z jeho tahů je cítit veliká zkušenost. Bude nanejvýš poučné s ním vypít šálek čaje. <br><br>
<iframe class="video" src="https://www.youtube.com/embed/S5lOysLYKyc?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe><br><br><br>


Mistr Černý Vichr je muž divokého charakteru. Jeden z nejproslulejších bojovníků beze zbraně v říši. O jeho neuvěřitelných dobrodružstvích se vyprávějí legendy. Mezi nejznámější patří povídání o tom, jak úplně sám a zcela nahý noc a den obléhal blíže nejmenovanou pevnost. Ať už je to pravda nebo ne, není pochyb o tom, že bude na turnaji silným protivníkem. I na desce volí vždy silné, agresivní tahy, oproti pomalé vytříbenosti a tato osoba bude velmi poctěna, dostane-li příležitost se s tímto mistrem utkat. <br><br>
<iframe class="video" src="https://www.youtube.com/embed/DrHpCaEv24c?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe><br><br><br>

Ani na okamžik jsem nepřestal doufat, že mistr Železná Yu se také zúčastní turnaje. Železná Yu svého času sloužila jako rádce místního pána Daimyo a vysloužila si pověst nekompromisní, ale spravedlivé ženy. Přes svůj malý vzrůst se u všech, co ji znají, těší velkému respektu a je jednou z našich nejváženějších znalců tradičního divadelního umění.
<br><br>
<iframe class="video" src="https://www.youtube.com/embed/9g0m0swmxGY?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe><br><br><br>

Mistr Tao Kan je v naši provincii méně známý hráč. Muž, o jehož hbitosti s mečem se vyprávěly legendy, který ale na dlouhou dobu, jako by zmizel z říše. Jeho návrat byl stejně nečekaný, jako lehkost s kterou porazil řadu známých, silných hráčů, a mně je velikou ctí ho uvítat na turnaji.
<br><br>
<iframe class="video" src="https://www.youtube.com/embed/FM7tIx8T8gM?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe><br><br><br>

Mistr Mandloňový květ poctí náš turnaj svou účastí. Kromě ladnosti svých tahů na desce je nejvíce známa odvážnými křivkami jejích znaků na papíře. <br><br>
<iframe class="video" src="https://www.youtube.com/embed/RJPyMtlItRU?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
<br><br><br>
</center>

  
  {{< endWrapper >}}







 
