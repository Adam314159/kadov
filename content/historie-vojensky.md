---
title: "Historie | "
date: 2019-04-20T10:05:17+02:00
draft: false
---


  {{< wrapper id="" title="2013 - Přežití videa" subtitle="" >}}
<center>
Následující video neměla být pouze komedie, ale pamatuje, prosím, na to, že to byla naše táborová video prvotina. Z videa, které jednotky dorazivší na Kadov našli, aby objasnilo osud původní skupiny vojáků.<br><br> 
<iframe class="video" src="https://www.youtube.com/embed/0d3ZJPpUdT0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe><br><br>

Vše co jsme se na táboře měli naučit:<br><br>
<iframe class="video" src="https://www.youtube.com/embed/1LOTSD3b4K8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe><br><br>

A ještě nepovedené záběry:<br><br>
<iframe class="video" src="https://www.youtube.com/embed/aV0aQXFeMLg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe><br><br>

</center>

  
  {{< endWrapper >}}