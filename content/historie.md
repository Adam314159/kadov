---
title: "Historie | "
date: 2019-04-20T10:05:17+02:00
draft: false
---


  {{< wrapper id="kontakt" title="Tábory dávno minulé" subtitle="" >}}
Věřte nebo ne, máme za sebou přes čtvrt století táborů *(Kdyby Vás zajímala podrobná historie, je sepsána <a href="/images/souhrn/tabory.pdf" target=_blank noreferrer noopener><b>zde</b></a>)*. Původní složení vedoucích se pochopitelně postupně <a href="/images/souhrn/hradce-kadov.xlsx" target=_blank noreferrer noopener>obměňuje</a>, ale myšlenka, myšlenka aby zůstala...
<br><a href="/pametnici" style="font-size: 12px; opacity: 0.2;">Tajná sekce pro pamětníky</a><br><br><br> 


<a href="#1998" class="timeline" tooltip=Návštěva&nbsp;ZOO style="width: 35px; color: white;">1998</a>
<a href="#1999" class="timeline" tooltip=Putování&nbsp;po&nbsp;zeměkouli>1999</a>
<a href="#2000" class="timeline" tooltip=Indiánské&nbsp;léto>2000</a>
<a href="#2001" class="timeline" tooltip=Ostrov&nbsp;pokladů>2001</a>
<a href="#2002" class="timeline" tooltip=Lovci&nbsp;mamutů>2002</a>
<a href="#2003" class="timeline" tooltip=Putování&nbsp;za&nbsp;skřítky>2003</a>
<a href="#2004" class="timeline" tooltip=Umění&nbsp;meče>2004</a>
<a href="#2005" class="timeline" tooltip=Osmitisícovky&nbsp;světa>2005</a>
<a href="#2006" class="timeline" tooltip=Antika>2006</a>
<a href="#2007" class="timeline" tooltip=Indiáni>2007</a>
<a href="#2008" class="timeline" tooltip=Král&nbsp;Artuš style="width: 35px; color: white;">2008</a>
<a href="#2009" class="timeline" tooltip=Japonsko>2009</a>
<a href="#2010" class="timeline" tooltip=Western>2010</a>
<a href="#2011" class="timeline" tooltip=Divadelní>2011</a>
<a href="#2012" class="timeline" tooltip=BlacKadov>2012</a>
<a href="#2013" class="timeline" tooltip=Přežití>2013</a>
<a href="#2014" class="timeline" tooltip=Námořnický>2014</a>
<a href="#2015" class="timeline" tooltip=Hvězdná&nbsp;brána>2015</a>
<a href="#2016" class="timeline" tooltip=Škola&nbsp;čar&nbsp;a&nbsp;kouzel&nbsp;v&nbsp;Kaďovicích>2016</a>
<a href="#2017" class="timeline" tooltip=Ostrov&nbsp;Šamanů>2017</a>
<a href="#2018" class="timeline" tooltip=Japonsko style="width: 35px; color: white;">2018</a>
<a href="#2019" class="timeline" tooltip=Ragnarök>2019</a>
<a href="#2020" class="timeline" tooltip=Zrušeno&nbsp;pro&nbsp;COVID>2020</a>
<a href="#2021" class="timeline" tooltip=Tajemný&nbsp;experiment>2021</a>
<a href="#2022" class="timeline" tooltip=Terra&nbsp;Mystica>2022</a>
<a href="#2023" class="timeline" tooltip=Vesmírná&nbsp;odysea>2023</a>
<a href="#2024" class="timeline" tooltip=Hrdinové&nbsp;Olympu>2024</a>
<br><br><br><br>
  {{< karta2 id="2024" jmeno="Hrdinové Olympu" autor="Barča" heslo="Společně zaženeme nestvůry zpátky tam, kam patří – do temných hlubin Tartaru!" foto="a.1076577714170562" video="historie-jackson" >}}  
Neuznaní podlou lest potajmu připravili, prastarou věštbu tak po věcích naplnili. Ztracený hrdina ze spánku procitá, neb jeho krev jest pro tvrdou mstu nezbytná. Krutá zrada se pokradmu chystá, obětí stane se duše přečistá. V lásce a věrnosti naděje zůstává, však duše hrdiny po pláních bloudívá. Poslední veliká bitva se blíží, zhoubou či smrtí nový věk klíčí.
  {{< /karta2 >}} 

 {{< karta2 id="2023" jmeno="Vesmírná Odysea" autor="Šimon" heslo="S Aliancí spojených planet pro lepší zítřky!" foto="a.839204724574530" video="historie-vesmirna" >}}  
Posádka mezihvězdné lodi se vydala na dlouhou cestu nebezpečným vesmírem. Na palubě je sice doprovází nejlepší astronauti, ale vesmír je nebezpečné místo. Neznámý svět, který je jejich cílem, může skrývat mnoho rizik.
  {{< /karta2 >}}

  {{< karta2 id="2022" jmeno="Terra Mystica" autor="Peťule" heslo="Živly, rituály, magie" foto="a.604304971397841" video="historie-terra" >}}  
Kouzelná země plná magie, rituálů a alchymie. Obyvatelé světa Terra Mystica jsou rozděleni do 16 národů lišící se vzhledem, schopnostmi a znalostmi. Každý z národů upřednostňuje jeden typ krajiny, který může osidlovat a vybudovat zde svůj domov. Krajina se však neustále mění s tím, jak v ní jednotlivé národy rozšiřují svá území. Podaří se nám zastavit ledové víly, než zmrazí celý svět?
  {{< /karta2 >}}

  {{< karta2 id="2021" jmeno="Tajemný experiment" autor="Adélka" heslo="Co všechno se může skrývat v jedné lahvičce podivné tekutiny?" foto="a.1515647828790297" video="historie-experiment" >}}  
V okolí vesnice Kadov se dějí podivné věci. Ztrácí se krůty, novináři neberou telefony a nikdo o tom nechce mluvit. To však v žádném případě neodradí investigativního vloggera Edu od pátraní. To, co nakonec odhalí, šokuje celý svět. 
  {{< /karta2 >}}
  
  {{< karta2 id="2020" jmeno="Prokletá expedice" autor="Šimon" heslo="Třeba se ještě vrátí" red="1" >}}  
Tato expedice byla natolik prokletá, že jsme ji bohužel kvůli epidemii zrušili.
  {{< /karta2 >}}
  
  {{< karta2 id="2019" jmeno="Ragnarök" autor="Stoupa" heslo="" foto="a.935634576791628" kronika="" >}}  
Severští bohové byli až příliš dlouho lehkomyslní a pošetilí. Thor, vládce hromu, z rozmaru zabíjel obry, Ódin si myslel, že je všemocný a bez váhání obětoval život svých věrných, aby si uchránil ten svůj, a Loki, bůh chaosu, tomu vesele přihlíží a využívá vše ve svůj prospěch. Věštba však praví, že tento svět zanikne, a to i se všemi bohy. Ódin dělá vše proto, aby Ragnaroku, konci světa, zabránil, kostky jsou však již vrženy.
  {{< /karta2 >}}
  
  {{< karta2 id="2018" jmeno="Japonsko" autor="Adam" heslo="Nepokládám dnes žádné otázky duši. Ať doufá v tichu." kronika="" foto="a.708713066150448" video="historie-japonsko" >}}  
V osadě Kadó voní čaj, čerstvě upečené sušenky a cestu osvětlují světla lampiónů. Proč, ptáte se? Koná se zde totiž turnaj v tradiční deskové hře Go a z celého Japonska se sem sjíždějí ti nejzkušenější hráči, aby zvítězili a mohli se utkat s naším pánem Šogunem. Gonosuke, náš hostitel, se turnaje účastní taktéž, nicméně brzy nám odhalí, co jej k organizování této slavnostní události vedlo… Dokážeme společně odhalit tři vrahy, kteří usilují Šogunovi o život?
  {{< /karta2 >}}

  {{< karta2 id="2017" jmeno="Ostrov Šamanů" autor="Šaman" heslo="" kronika="" foto="a.492813927740364">}}  
Šest kmenů, šest barev, šest smyslů, šest šamanů, jeden ostrov. Kmeny bojují každý den o život, a to nejen proti ostrovu, ale i proti sobě navzájem. Kdo přijde dnes o střechu nad hlavou a kdo o možnost získat jídlo ze společných zásob? Uhlídáš si svou vlajku i totem před ostatními kmeny? Šaman tě oslabí, oheň utichne a temná noc upadne na spící osadu. Zvítězí ostrov nebo ty? 
  {{< /karta2 >}}

  {{< karta2 id="2016" jmeno="Škola čar a kouzel v Kaďovicích" autor="Eliška" heslo="" kronika="" foto="a.322440611444364" video="historie-kadovice" >}}  
Vítejte ve škole čar a kouzel v Kaďovicích, kde je magie na denním pořádku! Každý čaroděj, kterému byl soví poštou zaslán jeho dopis, se naučí znát tajemství starodávných run, kouzelné formule, létání na koštěti nebo míchání lektvarů od těch nejpovolanějších profesorů v kouzelnickém světě. Obloha ale potemní, když se Malchor Atopus, vynikající kouzelník, zblázní a po nocích začne školu navštěvovat záhadná postava v kápi.  
  {{< /karta2 >}}

  {{< karta2 id="2015" jmeno="Hvězdná brána" autor="Venda" heslo="" kronika="" foto="a.696742604014161" video="historie-stargate">}}  
Jakákoliv podobnost se stejnojmenným seriálem je zcela náhodná! A jaká další nečekaná náhoda – nalezli jsme bránu do jiných světů. Putování napříč vesmírem, poznávání nových planet i jejich zvláštních obyvatel bylo snem každého z nás, dokud jsme nenarazili na Goaldy. Vesmírné příšery, jejichž cílem je najít si svého hostitele a pomocí jeho těla postupně ovládnout lidstvo, jeho svět i celý zbytek vesmíru. 
  {{< /karta2 >}}

  {{< karta2 id="2014" jmeno="Námořnický" autor="Stoupa a spol" heslo="Náš lodivod je zas jak kára." kronika="" foto="a.696753017346453">}}  
Šest posádek bojuje na širém moři o holý život. Jejich vory troskotají u břehů nejrůznějších ostrovů, neustále ale nalézají naději a potřebné vybavení, aby se dostali o kousek dál. Mapa je stará a roztrhaná, kompas nefunguje, zásoby rumu uplavaly a na námořníky pomalu padá mořská nemoc. V dálce jsou vidět jen chapadla obří mořské příšery, tři citrónky a truhla s pokladem.  
  {{< /karta2 >}}

  {{< karta2 id="2013" jmeno="Přežití" autor="Adam" heslo="Tábor mizerných video spotů (aneb jak začínala má amatérská láska k filmování)" kronika="" foto="a.696761204012301" video="historie-vojensky">}}  
Elitní jednotky dorazily do prostor vojenské základny Kadov, aby pomohli plukovníku Růžičkovi odhalit, co tají tamní domorodci a proč jsou prostory již tak dlouho opuštěné. Je potřeba nejdříve vojáky vycvičit, aby se byli schopni přizpůsobit stávající situaci – nikdo však nebyl řádně připraven na komunikaci s místními. Dovolí nám těžit vysoce nebezpečný a výbušný nerost? Nebo bychom jej raději měli rovnou použít na jejich zneškodnění?  A to ještě netuší, že mají zrádce ve vlastních řadách...
  {{< /karta2 >}}

  {{< karta2 id="2012" jmeno="BlacKadov" autor="Stoupa" heslo="" kronika="" foto="a.696769770678111">}}  
BlacKadov je malá kouzelnická osada ukrytá mezi korunami stromů nedaleko Icharydova jezera. Žijí zde v míru, pod vládou mocného Lorda Azmara, nejrůznější rasy a cechy a společně velebí svou Severní divokou pláň. Alchymistky se zdokonalují v míchání lektvarů, kouzelnice zase v kouzelných formulích, elfové zkoumají staré runy a lapkové a trpaslíci prahnou po zlatě. Jednoho dne se však probudí Icharyd, temný čaroděj, který za pomoci svých poskoků Ghoulů ovládne celý BlacKadov…  
  {{< /karta2 >}}

  {{< karta2 id="2011" jmeno="Divadelní" autor="Šaman" heslo="" kronika="" foto="a.696779417343813">}}  
Sokol hledá divadelní tvář! Herci a herečky ze všech koutů světa se sjeli do Kadovských ateliérů, aby rozšířili svůj talent a zahráli zde roli svého života. Čeká je nejen tvrdý trénink v podobě kaskadérství, výroby divadelních loutek, lekcí zpěvu a tance nebo dabingu, ale také sepsání vlastního scénáře a jeho zrealizování v co nejkratší možné době. Kdo získá za svou čtrnáctidenní roli Oscara?
  {{< /karta2 >}}

  {{< karta2 id="2010" jmeno="Western" autor="Opice" heslo="" kronika="" foto="a.696787220676366">}}  
Arizona, Texas, Dakota, Alabama, Tennessee a Colorado – to jsou státy, kde svůj den začnete přestřelkou s bandity, k obědu rozžvýkáte podkovy svých koní a na večer vypijete flašku rumu, abyste na celej ten zatracenej den zapomněli. Západ je drsnej! Teď však musí obyvatelé těchto měst dokázat, že se s jejich životním osudem umít rvát zuby nehty – číhá na ně nebezpečí při rýžování zlata, osvobozování otroků z dolů a také záhadná smrt jednoho moc starého zlatokopa.    
  {{< /karta2 >}}

  {{< karta2 id="2009" jmeno="Japonsko" autor="Adam" heslo="Srdce mi radí bych zřel jedinou z pravd. Oči jsou slepé." kronika="" foto="a.696802400674848">}}  
Zástupci starojaponských rodů v čele s místním Šogunem nadšeně očekávají příjezd vznešeného pána a vládce Daimió. Když v tom se skupince několika z nich zjeví duch zemřelého Yošika Misogiho z Mimaski. Podaří se vybraným samurajům a gejšám jít po stopách Yošika a odhalit, co za tajemství skrývá provincie Kadó?   
  {{< /karta2 >}}

{{< karta2 id="2008" jmeno="Král Artuš" autor="Mája" heslo="" kronika="" foto="a.696809354007486" video="historie-artus">}}  
Středověká města v království Artura Pendragona přihlížejí příběhu mladého prince Artuše. Artuš vytáhne meč z kamene, stane se králem, vezme si za ženu Guinevru  a společně s rytíři kulatého stolu se udatní rytíři a vznešené dámy budou veselit na hradě Kamelotu, dokud nenastane den bitvy Artuše s Mordredem, synem mocné čarodějky Morgany. Kouzelník Merlin, Artušův věrný přítel a rádce, si však pro obyvatele měst připravil ještě jednu dobrodružnou výpravu – cestu za Svatým Grálem! 
{{< /karta2 >}}

<br><br>
# Hradce, předchůdce Kadova
{{< karta2 id="2007" jmeno="Indiáni" autor="Stoupa" heslo="" kronika="">}}  
Spánek v Tee Pee, vystřelit hořící šíp či vykouřit dýmku míru? Komu by se život pod indiánskou čelenkou plnou per nelíbil? Může se zdát, že Rudé tváře mají život skutečně pohádkový, celé dny jen jezdí na koni a toulají se v divoké přírodě. Opak je však pravdou…
{{< /karta2 >}}
  
  {{< karta2 id="2006" jmeno="Antika" autor="Majka" heslo="" kronika="">}}  
V řeckém Pantheonu se v létě sejdou všichni známí bohové – Zeus, Hádes, Héra, Athéna, Dionýsos a další. Po boku těchto bohů se spolu utkají řecká města v nejrůznějších soutěžích.  Božský život však není jen o soupeření a válkách, je také o víně, ženách a zpěvu!
  {{< /karta2 >}}
    
  {{< karta2 id="2005" jmeno="Osmitisícovky světa" autor="Vlasta" heslo="" kronika="">}}  
„Severní pól je dobyt!“ Šest expedic se vydává na nebezpečnou cestu po hřebenech nejvyšších pohoří světa. Jejich úkolem je pokořit všechny osmitisícovky od nejnižší až po nejvyšší. Hory jsou však nevyzpytatelné, musí čelit mnohým překážkám a překonat vlastní nepohodlí. Všech šest členů každé z výprav se bude muset naučit spolupracovat, jinak bude život každého z nich v sázce. Vrátí se z vrcholů všichni? 
  {{< /karta2 >}}
  
  {{< karta2 id="2004" jmeno="Umění meče" autor="Šaman" heslo="" kronika="">}}  
Jen ti nejodvážnější a nejzdatnější šermíři se sjeli do našeho království, aby dobyli tucet hradů, které se zde nachází. Dokáží se poté o svůj hrad postarat, zajistit úrodu, starat se o lesy a louky? A kolik z nich se nakonec prokáže takovou udatností, aby mohli být pasováni na rytíře? 
  {{< /karta2 >}}

  {{< karta2 id="2003" jmeno="Putování za skřítky" autor="Houba" heslo="" kronika="">}}  
Ústřední hrou tábora bylo veliké člověče nezlob se, kde měl každý účastník své figurky a kde probíhaly nelítostné souboje.
  {{< /karta2 >}}

  {{< karta2 id="2002" jmeno="Lovci mamutů" autor="Majka" heslo="" kronika="">}}  
V čase souměrném v roce nového milénia… se tok času obrátí! A on je rok 2002! Souměrný rok! Světla v místnosti pohasla, otočil se takový divný vítr a do dveří vpadlo několik pravěkých lovců.     
  {{< /karta2 >}}

  {{< karta2 id="2001" jmeno="Ostrov pokladů" autor="Vlasta" heslo="" kronika="">}}  
Loď zakotvena v přístavu, je slyšet jen pleskot plachet ve větru, jinak ticho, tma. V podpalubí však námořníci prolévají svá hrdla rumem a zpívají z plných plic. Tu se v dáli objevila temná postava. Byl to kapitán Flint. Mluvil hlubokým hlasem a za sebou táhl svou dřevěnou nohu...     
  {{< /karta2 >}}

  {{< karta2 id="2000" jmeno="Indiánské léto" autor="Šaman" heslo="" kronika="">}}  
Na jednom místě žijí čtyři indiánské kmeny: Černonožci, Vraní indiáni, Dakotové a Inkové. Zprvu v dobrém sousedství, ale pak vypuknou mezi kmeny rozbroje. Podaří se je náčelníkům zažehnat a vykouří společně dýmku míru? 
  {{< /karta2 >}}
  
  {{< karta2 id="1999" jmeno="Putování po zeměkouli" autor="Šaman" heslo="" kronika="">}}  
Willy Fog stihl celou zeměkouli procestovat za 80 dní, s naší mapou to stihnete za pouhých 14! Poznejte nejrůznější kouty světa...     
  {{< /karta2 >}}
  
  {{< karta2 id="1998" jmeno="Návštěva ZOO" autor="" heslo="" kronika="">}}  
Vydejte se na cestu zoologickou zahradou a potkejte hravé lachtany, dikobraza střelce, sovu pamatováka, opičí dráhu nebo slona ve vodě.     
  {{< /karta2 >}}

  {{< endWrapper >}}
