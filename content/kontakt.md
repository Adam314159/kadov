---
title: "Kontakt | "
date: 2019-04-20T10:05:17+02:00
draft: false
---

{{< wrapper id="kontakt" title="Kontakt" subtitle="" >}}
Potřebujete se na něco zeptat?
<br><br> 

<h2>Hlavní vedoucí</h2>

Marek Pitra

703 847 316

*marek.pitra@centrum.cz*

<br> 

<h2>Zdravotník</h2>

MUDr. Petr Kazda

*kazda.p@centrum.cz*

<br> 

<h2>Adresa tábořiště</h2>

Tábor TJ Sokol České Budějovice<br>
tábořiště Pod farou<br>
387 33 Kadov u Blatné<br>


  
  {{< endWrapper >}}







 
