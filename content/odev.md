---
title: "Celotáborovka | "
date: 2019-04-20T10:05:17+02:00
draft: false
layout: celotaborovka_empty
---


{{< wrapper id="celotaborovka" title="" subtitle="" background="1" >}}

<div style="width: 100%; background: rgb(232,212,200); padding: 40px; border-radius: 2px; margin: 0 auto; text-align: left;">
<a href="/celotaborovka"><b><- Zpět</b></a><br><br>

<center><h1>Oděv</h1></center>

Jak se vlastně na výpravu vhodně obléci? Nacházíme se v roce 1912 ve Spojených státech. Oblečení by mělo být pohodlné, ale zároveň společensky přijatelné! Mezi části společné pro obě pohlaví patří košile, kšandy jsou velmi slušivým bonusem. Stejně tak dobře padnoucí vestička přidá kredit jak dobrodruhům, tak cestovatelkám.<br><br>

<a href="/images/1.jpg" data-lightbox="priroda" class="animate"><img src="/images/stul/fotka1.png" style="display: inline-block;"></a>
<a href="/images/oblek2.jpg" data-lightbox="priroda" class="animate"><img src="/images/oblek2.jpg" style="display: inline-block; width: 20%; margin-left: 40px; transform: rotate(5deg)"></a>
<a href="/images/oblek3.jpg" data-lightbox="priroda" class="animate"><img src="/images/oblek3.jpg" style="display: inline-block; width: 20%; margin-left: 50px; transform: rotate(-10deg)"></a>


<br><br>

Na obrázku mají všechny cestovatelky sukni, ale Spojené státy jsou pokroková země! Cítí-li se nějaká slečna emancipovaně, kalhoty jsou dobrá volba.
Nezbytnou součástí každé garderóby by měl být také klobouk pro dobrou ochranu před sluncem.

</div>
	
{{< endWrapper >}}






 
