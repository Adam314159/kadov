---
title: "O vedoucích | "
date: 2019-04-20T10:05:17+02:00
draft: false
---


{{< wrapper id="nas_tym" title="Náš tým" subtitle="" >}}
<a href="/images/foto/team.jpg" data-lightbox="priroda" class="animate inline-block"><img src="/images/foto/nahl/team.png"></a>

Dovolte mi představit Vám všechny členy našeho týmu (řazeno dle služebního stáří). Po dobu tábora je každý z nás stoprocentní nekuřák a abstinent.
Znám je už přes deset let (mnohé i déle) a za každého z ních ručím osobně.<br><br>
  
  {{< karta img="stoupa" jmeno="Jiří Stančík" prezdivka="zvaný Stoupa" prace="Mistr logistiky v Groz-Beckert" heslo="Dlouhé roky tvrdil, že uběhne půlmaraton. Ukázalo se, že nelhal." >}}  
Náš služebně nejstarší a tím pádem asi i&nbsp;nejzkušenější vedoucí. Už jako šestiletý chlapec jezdil na Hradce, což byl předchůdce našeho kadovského tábora, kde se z&nbsp;hrozného lumpa vypracoval v&nbsp;čestného mladého muže. Je neskutečně soutěživý, což se odráží i&nbsp;v&nbsp;jeho nadšení do vymýšlení různých deskových i&nbsp;karetních her, jejichž pravidla vám dokáže se zápalem vysvětlovat klidně i&nbsp;několik hodin.	
  {{< /karta >}}  

<!--  
  {{< karta img="adam" jmeno="Adam Růžička" prezdivka="to jsem já" prace="3D designér a animátor" heslo="Umí nakreslit nejlepší črtu " samurai="1" >}}  
Další z hradeckých dětí. Na rozdíl od Jířího Stančíka byl hodné dítě už od začátku. Umí z&nbsp;paměti vyjmenovat Ludolfovo číslo do 50 desetinných míst a přednést báseň Krkavec v&nbsp;originálním znění. Nic víc se v životě nenaučil.
  {{< /karta >}}  
-->
  
  {{< karta img="marek" jmeno="Marek Pitra" prezdivka="zvaný Mára" prace="Nějaké složité inženýrství v BOSCHi nebo co" heslo="Ráno kněz, přes den inženýr, v noci bůh básnictví." >}}  
Mára byl na našem táboře chvíli instruktorem, chvíli vedoucím a na nějaký čas si dokonce střihl roli i hlavního vedoucího. Potom fungoval jako takový náš spasitel – přijel na tři dny na návštěvu, vyřešil všechny naše problémy a ještě k tomu zajistil světový mír. Poslední roky ho můžete vidět na Kadově zase zpátky v klubovně vedoucích a my bychom z toho nemohli mít větší radost! Marek je vždy připraven improvizovat, moc rád se nechá vyzvat na nelítostný souboj v Osadnících (ale nesmí u toho být jeho žena) a při večerním ohni ladí atmosféru procítěnou recitací básniček od Karla Plíhala. 
  {{< /karta >}} 
  
  {{< karta img="venda" jmeno="Václav Čaloun" prezdivka="prostě Venda" prace="IT technik" heslo="Legenda praví, že levou rukou uzvedne tři děti." >}}  
Venda je u dětí nesmírně oblíbený, nikdo z nás ale ještě nezjistil proč. Je pověstný svou zálibou v&nbsp;jakékoli (pyro)technice – kdybyste mu prošacovali kapsy, najdete tam minimálně jeden nadýmáček, kvalitní reproduktor, vysílačku, prodlužku a&nbsp;neznámé zařízení, o&nbsp;kterém sice nebudete vědět, k&nbsp;čemu slouží, ale Véna vás přesvědčí, abyste si je také pořídili. Je to zodpovědný mladý muž, který bude radši celou noc ležet tváří k zemi v&nbsp;mrznoucím bahně, než aby zkazil hru. 
  {{< /karta >}} 

  {{< karta img="nesty" jmeno="Jan Nestával" prezdivka="zvaný Nesty" prace="Nějaké inženýrství v Temelíně (prý se nemusíme bát) " heslo="Člověk, který se nezeptá jak daleko, když mu řeknete, aby běžel." >}}  
Nesty platí v&nbsp;našem týmu za chodící kalkulačku – ani byste nevěřili, jak je užitečné mít vedle sebe člověka, který vám z&nbsp;hlavy vypočítá, kolik gramů paštiky potřebujete vzít na celodenní výlet, abyste nakrmili celý tábor. Je až nezvykle multifunkční, dokáže zabavit jak patnáctileté kluky, tak malé holčičky, umí vyrobit nekonečně mnoho měkčených zbraní a&nbsp;sám vyniká hlavně ve střelbě z luku. Má rád poníky a&nbsp;textový font Comic Sans.   {{< /karta >}} 

<!--   
  {{< karta img="adelka" jmeno="Adéla Boháčová" prezdivka="zvaná Rejža" prace="Studentka" heslo="I v létě chodí spát v čepici, aby jí netáhlo na padawanský copánek." >}}  
Adélka měla už jako dítě vždy hlavní slovo v&nbsp;oblasti našich táborových divadelních představení, hravě zastane funkci režisérky, hlavní herečky, kostymérky i&nbsp;rekvizitářky zároveň. Za vrchol své kariéry považuje zdramatizování receptu na kuřecí polévku. Áďa má mnoho přezdívek, konkrétněji od každého písmene abecedy jednu, ráda vaří (a ví, že to umí) a&nbsp;když jí hodíte míč, zaručeně ho nechytí.  
  {{< /karta >}} 
-->
  
  {{< karta img="simon" jmeno="Šimon Havíř" prezdivka="zvaný Sebík" prace="Teď už INŽENÝR " heslo="Na táboře vstává o hodinu dřív, aby si mohl jít zaběhat." >}}  
Neuvěřitelně hodný mladý muž, který by pro druhé udělal cokoliv – i kvůli tomu je v našem týmu takovým pokusným králíkem, na němž se testují všechny náročné nebo nebezpečné aktivity. Rád vypráví matematické a chemické vtipy (kterým se nikdo nesměje) a společně s Adamem se stará o kultivovanost našich jazykových projevů tím, že nám neustále opravuje gramatiku. Je trochu perfekcionista a nedá ničemu a nikomu víc než 8 bodů z 10. 
  {{< /karta >}} 
  
<!--
  {{< karta img="petule" jmeno="Petra Nestávalová" prezdivka="zvaná Peťule" prace="Studentka " heslo="Lék na deprese." >}}  
Kdybyste prohledali celý vesmír, nenajdete pozitivnějšího stvoření, než je naše Peťa. Umí naprosto přesně odhadnout, kdy potřebujete obejmout, ale zároveň, kdy je potřeba zakročit a&nbsp;zvýšit hlas. Kvůli její výšce si jí ostatní často pletou s&nbsp;dětmi, přitom je z&nbsp;nás duchem ta nejdospělejší. Hudební projev u&nbsp;táborového ohně pozvedla na velmi vysokou úroveň svým hraním na akordeon, díky čemuž má bicepsy větší než většina našeho pánského osazenstva.
  {{< /karta >}}
-->  

  {{< karta img="barca" jmeno="Barbora Filipová" prezdivka="" prace="Studentka " heslo="Jakmile si vás zamiluje, dosáhli jste ve svém životě nejvyšší možné úrovně." >}}  
Neuvěřitelně pilná dáma, díky níž mají všechny slečny na našem táboře celých 14 dní účesy jako ze salónu. Barunka je nesmírně laskavá, obětavá a cílevědomá. Ačkoliv je po většinu času velmi tichá, ukrývá se v ní kreativní duše plná báječných nápadů – umí krásně kreslit, šít si vlastní kostýmy a&nbsp;zdobit táborové nástěnky. Platí za hlavní kytaristku našeho amatérského orchestru, některé děti ji považují za ztělesněný jukebox. 
  {{< /karta >}}   
  
  {{< karta img="julca" jmeno="Iva Šimková" prezdivka="zvaná Julča" prace="Studentka" heslo="Neodmítne meloun v jakémkoli skupenství." >}}  
Nadšená fotografka, milovnice českého jazyka, a&nbsp;hlavně ta nejmíň zkažená duše našeho tábora. Julča se do našeho týmu přidala o něco později než její vrstevníci, nicméně zkušenosti s&nbsp;dětmi nasbírala během let, kdy pomáhala vést náš českobudějovický oddíl pobytu v přírodě. Umí složit dinosaura z papíru, pozná každou bylinu a&nbsp;dřevinu v&nbsp;okruhu pěti kilometrů a&nbsp;je schopná z&nbsp;jediné vyřčené věty poznat, zda nejste masový vrah.  
  {{< /karta >}}

  {{< karta img="mates" jmeno="Matthias Pribik" prezdivka="" prace="Student" heslo="Rád kope nepřirozeně hluboké jámy v zemi. Brum, brum." >}}  
Kadov je pro Matyáše tak silnou srdcovou záležitostí, že mu nevadilo si na volné místo u vedoucovského stolu počkat několik let a nyní je ochoten za námi dojíždět až z Německa, kde bydlí a&nbsp;studuje, i&nbsp;když musí celých 14 dní spát s&nbsp;pokrčenýma nohama, protože se nevejde do podsadového stanu. Sice vám bude tvrdit, že správně se švihadlu říká skákací guma, také si nepamatuje jména svých oddílových dětí, ale zato je poslušnější než většina z&nbsp;nich.  
  {{< /karta >}} 

<br><br><br>
# Nová krev
Jelikož (samozřejmě kromě našich dámských vedoucích) nemládneme, řekli jsme si, že je načase najít mladé talenty, abychom si udrželi menší než depresivní věkový průměr. 

{{< karta img="ivca" jmeno="Iva Zábranská" prezdivka="Ivča" prace="Studentka" heslo="" >}}
Aby se nám to nepletlo, přibrali jsme do týmu další Ivču. Jako ta první je i tahle naprosto nepostradatelná. Ivča je nadšená sportovkyně, čímž se stává těžkou konkurencí pro Šimona a Stoupu, s nimiž se nebojí si ráno ještě před budíčkem dávat závody v běhu.
{{< /karta >}}

{{< karta img="evca" jmeno="Eva Kerlesová" prezdivka="" prace="Studentka" heslo="" >}}
Z nejtiššího dítěte, jaké jsme kdy na táboře měli, nám vyrostla neskutečně úžasná instruktorka Evička! Její silnou stránkou bude zajisté schopnost organizace, protože tu uplatňuje již během roku na našem dětském oddíle nebo na Majálesu, největším českobudějovickém festivalu na světě. Je citlivá, empatická a s Ivčou tvoří nerozlučnou dvojku, která na našem táboře nemá konkurenci.
{{< /karta >}}

{{< karta img="verca" jmeno="Veronika Hejdová" prezdivka="Veverča" prace="Studentka" heslo="" >}}
Veverča, jak Verunce s oblibou přezdívá náš hlavní vedoucí, je největším srdcařem a nadšencem našeho nového instruktorského osazenstva. Jde do všeho naplno a už nyní umí o věcech přemýslet s takovým přehledem, že by jí to mohl závidět leckterý zkušený vedoucí. Poznáte jí podle nikdy nechybějícího úsměvu na tváři, který vám rozzáří i ten nejtemnější den.
{{< /karta >}}

{{< karta img="nelca" jmeno="Nelča Pechoušková" prezdivka="" prace="Studentka" heslo="" >}}
Pro Nelču si náš hlavní vedoucí vymyslel speciální přezdívku Salmonela, která s ní ale nemá vůbec nic společného - Nelču totiž chcete mít! Má srdce na dlani a stejně jako svá objetí ho často rozdává všem kolem sebe s vřelostí a&nbsp;úsměvem. Umí podpořit, zorganizovat i zvýšit hlas, což jsou obzvláště mezi (už ne tak moc) nováčky nedocenitelné dovednosti.
{{< /karta >}}

{{< karta img="pepa" jmeno="Josef Doležal" prezdivka="" prace="Student" heslo="" >}}
Ačkoliv je Pepa už poměrně dospělý mladý muž, pro mnohé z nás bude vždy tím malým roztomilým Pepíčkem, který byl naše první a nějakou chvíli i jediné dítě v našem novém oddíle. Od té doby se přes raubíře vypracoval na neskutečně obětavého a všímavého instruktora, který bude Vendovi šlapat na paty v soutěži o nejoblíbenějšího vedoucího. Pepa se vyznačuje zejména ochotou a naštěstí k tomu patří i ochota udělat si srandu sám ze sebe, což je v našem týmu obzvláště důležité.
{{< /karta >}}

{{< karta img="anicka" jmeno="Anička Škaroupková" prezdivka="" prace="Studentka" heslo="" >}}
Anička je neuvěřitelně kreativní osůbka. Každý její nápad a hra je unikát. Například vyrobit během poledňáku z lavoru ruletu není žádný problém! Z Aničky neustále srší pozitivní energie, o kterou ji žádné dítě nedokáže připravit. Její kadovské motto zní: "Pořádek dělá (ne)přátele!"
{{< /karta >}}

{{< karta img="anezka" jmeno="Anežka Doležalová" prezdivka="" prace="Studentka" heslo="" >}}
Byť je Anežka na pohled drobná bytost, její uplatnění v našem týmu rozhodně drobné není! Je to velice empatická slečna, které se nemá problém žádné dítě dojít svěřit. Trpělivějšího a klidnějšího člověka byste jen těžko hledali. Pokud byste nemohli najít sudou ponožku nebo třeba ešus, nemusíte smutnit! Stačí se zeptat Anežky (hlavně pokud jste malý kluk). A to nejlepší na závěr - kdyby se Vám ji nějakým zázrakem podařilo rozzlobit, na udobření Vám postačí 20 korun - Crazy Wolf a pomerančový Tic Tac je zkrátka sázka na jistotu!
{{< /karta >}}

<br><br><br>


# To není vše  
Zatímco my si hrajeme s dětmi, tyto osoby dělají všechnu tu důležitou práci.
 
<a href="/images/foto/kuchyne.jpg" data-lightbox="priroda" class="animate inline-block"><img src="/images/foto/nahl/kuchyne.png"></a>


 
{{< karta img="zub" jmeno="Radim Sejk" prezdivka="zvaný Zub" prace="Programátor inkubátorů" heslo="Jednou na tábor programoval bombu." >}}  
Náš pan hospodář, nákupčí a obstaravatel veškeré elektroniky a&nbsp;technického vybavení. Zatím jsme ani nezkrachovali, ani nevyhořeli, takže na něm něco bude. Zub umí profesionálně sundávat těžké hrnce z&nbsp;plotny, nabrousit věci, které vůbec nemusí být ostré, a&nbsp;uvaří vám kvalitní kávu nebo čaj i&nbsp;během apokalypsy. Bez řízku k&nbsp;obědu není v&nbsp;pátek ochotný vylézt z postele.    
{{< /karta >}} 
 
{{< karta img="petr" jmeno="Petr Kazda" prezdivka="" prace="Praktický lékař" heslo="Umí vyléčit i dítě, které bylo ve skutečnosti zdravé." >}}  
Petr už dlouhé roky obětavě a trpělivě chrání vaše děti jako náš táborový doktor (nyní ve spolupráci s Peťaskou). Je jediný v ČR kdo skutečně chápe pravidla deskové hry Eclipse, kterou je schopný hrát bez přestávky asi deset dní. 
  {{< /karta >}}
  
{{< karta img="peta" jmeno="Petra Sukdolová" prezdivka="zvaná Peťaska (podle vzoru bandaska)" prace="Studentka medicíny" heslo="Umí latinsky vyjmenovat každou kost v těle." >}}  
Každý kdo se rozhodne studovat medicínu je pro mě blázen, ale v tom nejlepším slova smyslu. I kdyby Vaše děti měly ten největší možný strach z doktorů, tak Peťasky se bát nemusí a určitě ani nebudou. Spíš se obáváme toho, že za ní budou chtít pořád chodit i děti, kterým ale vůbec nic není. 
{{< /karta >}} 
 
  
<!--
{{< karta img="elis" jmeno="Eliška Hlaváčková" prezdivka="Bc. et budoucí jinej Bc." prace="Studentka" heslo="Když alespoň jednou denně nezamete klubovnu, spustí se jí tik v levém oku." >}}Eliška se asi už jako dítě rozhodla, že umí prostě úplně všechno (a asi je to tak) a tak si tábor užila od účastníka, přes instruktora, několik let dokonce jako hlavní vedoucí a letos se rozhodla, že nám ukáže jak se to dělá v kuchyni. My co víme, v které budějovické kavárně Eliška během studia vaří nejen hamburgery, jsme si mohli ověřit, že je na co se těšit! Navíc umí správně užívat přechodníky v psaném i mluveném projevu, ráda moralizuje a když se napije kafe, je neskutečně milá. Před spaním si čte básničky a někdy i skládá haiku.
{{< /karta >}}

{{< karta img="eliskav" jmeno="Eliška Vacková" prezdivka="" prace="Studentka" heslo="" >}}  
Eliška je naše nová pomocnice do kuchyňského týmu. Má dechberoucí schopnost najít na každém to hezké a vždycky se stará nejdříve o druhé a až pak o sebe. Je mistrem diskuzí na jakékoliv téma a přeborníkem ve společenských tancích. 
{{< /karta >}} 



-->
  
{{< endWrapper >}}







 
