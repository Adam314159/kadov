---
title: "Vyšetřování #1 | "
date: 2019-04-20T10:05:17+02:00
draft: false
layout: celotaborovka_empty
---


{{< wrapper id="celotaborovka" title="" subtitle="" background="1" >}}

<div style="width: 100%; background: rgb(232,212,200); padding: 40px; border-radius: 2px; margin: 0 auto; text-align: justify;">
<a href="/celotaborovka"><b><- Zpět</b></a><br><br>

<center><h1 class="leading-none mb-8">Záhadný dopis</h1></center>

Když mi přišla poštou fotka, nejdřív jsem netušil, co to může znamenat. Až po chvíli mi došlo, na co se to koukám – Ztracená výprava! Tady v Arkhamu ji zná každý. V roce 1907 se sebrala skupina vědců z Miskatonické univerzity a vrátila se jich sotva třetina. A ani ta nedopadla nijak slavně. Když jsem četl přiložený dopis, moje ohromení jen vzrostlo.
<br><br><br>

<div class="text-justify text-green-900" style="font-family: 'PT Mono', monospace; background: whitesmoke; padding: 20px; border-radius: 5px;">

<b>Vážený pane Andersone,
obracím se na Vás, protože vím, že do Vaší kanceláře poslední dobou dochází jen uklízečka. Mám pro Vás nabídku. Myslím, že velice dobře víte, na co se koukáte. Jistí lidé by ocenili, kdyby byl objasněn osud této skupiny. Věřím, že budete dále vědět, co dělat. Veškeré finance vám budu uhrazovat pomocí anonymních šeků. Malá hotovost je doufám dostatečnou motivací.


<div class="text-right w-full text-green-900" style="font-family: 'PT Mono', monospace;"><br>

S upřímnými pozdravy.</b>
</div>
</div><br>
<img src="/images/1.jpg">
<br><br>
Ale podpis chyběl.	
{{< endWrapper >}}

</div>






 
