---
title: "Vyšetřování #2 | "
date: 2019-04-20T10:05:17+02:00
draft: false
layout: celotaborovka_empty
---


{{< wrapper id="celotaborovka" title="" subtitle="" background="1" >}}

<div style="width: 100%; background: rgb(232,212,200); padding: 40px; border-radius: 2px; margin: 0 auto; text-align: justify;">
<a href="/celotaborovka"><b><- Zpět</b></a><br><br>

<center><h1 class="leading-none mb-8">Původní výprava</h1></center>

Moje první kroky přirozeně vedly na Miskatonickou univerzitu – místo, odkud v roce 1907 vyrazila ona proslulá výprava. Elita celé fakulty... a podívejte se, jak to s ní dopadlo. Z výroční zprávy jsem dal dohromady alespoň několik účastníků. V čele expedice stál uznávaný profesor Jonas Reynolds, docela zvláštní figura. Z řad jeho kolegů jmenujme profesora Anthona Bayerse, doktorku Amalii Robertsovou a samozřejmě nebohého Johna Chandlera. Nejedná se o kompletní seznam členů původní expedice, ale jejich osudy rozhodně stojí za zmínku.
<br><br><br>





<div class="text-left text-green-900" style="font-family: 'PT Mono', monospace; background: whitesmoke; padding: 0px; border-radius: 5px; vertical-align: top; transform: rotate(0.5deg);">

<div class="block w-full lg:w-1/4 lg:inline-block">
<img src="/images/stul/1.png" style="transform: rotate(10deg);">
<img src="/images/stul/clip.png" style="position: absolute; left: -103px; top:-100px; transform: scale(.4, .4) rotate(-15deg);">
</div>

<div class="block w-full lg:w-3/5 lg:inline-block pl-4" style="text-align: left; vertical-align: top; padding-top: 20px; padding-bottom: 20px;">
<b>
Profesor Jonas Reynolds je od odchodu expedice nezvěstný. Žádný z navrátivších se vědců nevěděl, co se mu stalo. Jeho osud zůstává i nadále záhadou. Před rokem byl prohlášen za mrtvého.
</b>
</div>
</div><br>






<div class="text-left text-green-900" style="font-family: 'PT Mono', monospace; background: whitesmoke; padding: 0px; border-radius: 5px; vertical-align: top; transform: rotate(-0.5deg);">

<div class="block w-full lg:w-1/4 lg:inline-block">
<img src="/images/stul/2.png" style="transform: rotate(-3deg);">
<img src="/images/stul/clip2.png" style="position: absolute; left: -100px; top:-93px; transform: scale(.4, .4);">
</div>

<div class="block w-full lg:w-3/5 lg:inline-block pl-4" style="text-align: left; vertical-align: top; padding-top: 20px; padding-bottom: 20px;">
<b>
Doktorka Amalia Robertsová je od odchodu expedice nezvěstná. Nikdo neví nic o jejím osudu. Naposledy byla viděna u události, které ostatní přezdívají Rozdělení expedice. Bližší informace nikdo neposkytl. Před rokem byla prohlášena za mrtvou.
</b>
</div>
</div><br>


<div class="text-left text-green-900" style="font-family: 'PT Mono', monospace; background: whitesmoke; padding: 0px; border-radius: 5px; vertical-align: top; transform: rotate(-1deg);">

<div class="block w-full lg:w-1/4 lg:inline-block">
<img src="/images/stul/4.png" style="transform: rotate(5deg);">
<img src="/images/stul/clip.png" style="position: absolute; left: -100px; top:-96px; transform: scale(.4, .4);">
</div>

<div class="block w-full lg:w-3/5 lg:inline-block pl-4" style="text-align: left; vertical-align: top; padding-top: 20px; padding-bottom: 20px;">
<b>
Anthon Bayers se vrátil spolu s přeživšími členy expedice v dobrém fyzickém zdraví. Během povinné hospitalizace bylo zjištěno, že ho sužují noční můry. Křičel ze spaní slova v neznámém jazyce a vyhrožoval smrtí osobě, jejíž identitu se dosud nepodařilo určit. Necelý měsíc po návratu se odstěhoval z města, nikdo neví kam.
</b>
</div>
</div><br>


<div class="text-left text-green-900" style="font-family: 'PT Mono', monospace; background: whitesmoke; padding: 0px; border-radius: 5px; vertical-align: top; transform: rotate(0.7deg);">

<div class="block w-full lg:w-1/4 lg:inline-block">
<img src="/images/stul/clip2.png" style="position: absolute; left: -100px; top:-96px; transform: scale(.4, .4) rotate(15deg);">
<img src="/images/stul/3.png">
</div>

<div class="block w-full lg:w-3/5 lg:inline-block pl-4" style="text-align: left; vertical-align: top; padding-top: 20px; padding-bottom: 20px;">
<b>
John Chandler je ze všech účastníků expedice největším otazníkem. Necelý měsíc po návratu byl hospitalizován v Sanatoriu Panny Marie pro duševně choré. Od té doby nepromluvil ani slovo a napadl čtyři ošetřující lékaře.
</b>
</div>
</div><br>






{{< endWrapper >}}

</div>






 
